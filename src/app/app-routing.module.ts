import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'tomatican', loadChildren: './recetas-chilenas/tomatican/tomatican.module#TomaticanPageModule' },
  { path: 'porotos-granados', loadChildren: './recetas-chilenas/porotos-granados/porotos-granados.module#PorotosGranadosPageModule' },
  { path: 'tortilla-de-porotos-verdes', loadChildren: './recetas-chilenas/tortilla-de-porotos-verdes/tortilla-de-porotos-verdes.module#TortillaDePorotosVerdesPageModule' },
  { path: 'ceviche', loadChildren: './recetas-peruanas/ceviche/ceviche.module#CevichePageModule' },
  { path: 'lomo-saltado', loadChildren: './recetas-peruanas/lomo-saltado/lomo-saltado.module#LomoSaltadoPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
