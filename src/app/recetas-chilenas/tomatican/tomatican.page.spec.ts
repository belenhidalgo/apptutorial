import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TomaticanPage } from './tomatican.page';

describe('TomaticanPage', () => {
  let component: TomaticanPage;
  let fixture: ComponentFixture<TomaticanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TomaticanPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TomaticanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
