import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PorotosGranadosPage } from './porotos-granados.page';

describe('PorotosGranadosPage', () => {
  let component: PorotosGranadosPage;
  let fixture: ComponentFixture<PorotosGranadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PorotosGranadosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PorotosGranadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
