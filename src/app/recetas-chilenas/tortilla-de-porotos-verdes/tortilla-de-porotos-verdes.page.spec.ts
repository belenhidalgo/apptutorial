import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TortillaDePorotosVerdesPage } from './tortilla-de-porotos-verdes.page';

describe('TortillaDePorotosVerdesPage', () => {
  let component: TortillaDePorotosVerdesPage;
  let fixture: ComponentFixture<TortillaDePorotosVerdesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TortillaDePorotosVerdesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TortillaDePorotosVerdesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
