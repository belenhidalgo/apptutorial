import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TortillaDePorotosVerdesPage } from './tortilla-de-porotos-verdes.page';

const routes: Routes = [
  {
    path: '',
    component: TortillaDePorotosVerdesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TortillaDePorotosVerdesPage]
})
export class TortillaDePorotosVerdesPageModule {}
