import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LomoSaltadoPage } from './lomo-saltado.page';

const routes: Routes = [
  {
    path: '',
    component: LomoSaltadoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LomoSaltadoPage]
})
export class LomoSaltadoPageModule {}
