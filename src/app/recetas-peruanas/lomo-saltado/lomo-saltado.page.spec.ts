import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LomoSaltadoPage } from './lomo-saltado.page';

describe('LomoSaltadoPage', () => {
  let component: LomoSaltadoPage;
  let fixture: ComponentFixture<LomoSaltadoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LomoSaltadoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LomoSaltadoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
